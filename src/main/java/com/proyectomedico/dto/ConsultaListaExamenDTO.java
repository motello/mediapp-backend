package com.proyectomedico.dto;

import java.util.List;

import com.proyectomedico.model.Consulta;
import com.proyectomedico.model.Examen;


// un DTO sirve para poder simplificar operaciones que dependan de muchas entidades sin alterar las clases modelos
//para cuando tengamos varias consultas o reportes, crear un DTo para representar la data de salida 
public class ConsultaListaExamenDTO {

	private Consulta consulta;
	private List<Examen> lstExamen;
	
	
	public Consulta getConsulta() {
		return consulta;
	}
	public void setConsulta(Consulta consulta) {
		this.consulta = consulta;
	}
	public List<Examen> getLstExamen() {
		return lstExamen;
	}
	public void setLstExamen(List<Examen> lstExamen) {
		this.lstExamen = lstExamen;
	}
	
	
	
}
