package com.proyectomedico.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.proyectomedico.exception.ModeloNotFoundException;
import com.proyectomedico.model.Especialidad;
import com.proyectomedico.service.IEspecialidadService;

@RestController
@RequestMapping("/especialidades") //el acceso a esta clase sera con un sustantivo slash especialidad 


public class EspecialidadController {

	@Autowired
	private IEspecialidadService service; //para poder acceder a las operacioens de las capas inferiores, en relaidad estoy tomando la implementacion
	
	/*
	 * CON ESTE ORGININALMENTE FUNCIONA PERO SE PUEDE IMPLEMENTAR COMO ESTA ABAJO
	 * 
	@GetMapping (produces = MediaType.APPLICATION_JSON_VALUE) //indico que produce este servicio
	public List<Especialidad> listar(){
		List<Especialidad> especialidad = new ArrayList<>();
		especialidad = service.listar();
		return especialidad; 
	}
	*/
	
	@GetMapping (produces = MediaType.APPLICATION_JSON_VALUE) //indico que produce este servicio
	public ResponseEntity<List<Especialidad>> listar(){ //clase responseentity puedo manipular el estatus code del servicio, es un conjunto de numeros ok es 200
		List<Especialidad> especialidad = new ArrayList<>();
		especialidad = service.listar();
		return new ResponseEntity<List<Especialidad>>(especialidad, HttpStatus.OK); // es el 200
	}
	
	//listar por id
	@GetMapping(value = "/{id}")
	public Resource<Especialidad> listarId(@PathVariable("id") Integer id) {
		Especialidad esp = service.listarId(id);
		if (esp ==null) {
			throw new ModeloNotFoundException("ID: " + id); //en lugar de hacer lo de las excepciones por aca mejor envio el mensaje a la clase ModeloNotFoundException que los tienen todos 
		} 
		
	//a continuacion sera para implementar el HATEOAS
		
		Resource<Especialidad> resource = new Resource<Especialidad>(esp);
		ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).listarId(id));
		resource.add(linkTo.withRel("espiente-resource"));
		
		//return new ResponseEntity<Especialidad>(HttpStatus.OK); 
		return resource;
		
	}
	
	
	@PostMapping (consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)//consume es que este servicio recibe una trama json desde angular 
	public ResponseEntity<Object>registrar(@Valid @RequestBody Especialidad espiente){ //valid que respete los constraint que definimos en la Entity
		Especialidad esp = new Especialidad();
		esp = service.registrar(espiente); //le paso lo que viene despues del RequestBody
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(esp.getIdEspecialidad()).toUri(); //definir la ruta que genera el recurso de espiente
		return ResponseEntity.created(location).build(); //location es la url del recurso que usamos para esa insercion
	}
	
	
	@PutMapping (consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)//consume es que este servicio recibe una trama json desde angular 
	public ResponseEntity<Object>actualiza(@Valid @RequestBody Especialidad espiente){ //valid que respete los constraint que definimos en la Entity
		Especialidad esp = new Especialidad();
		esp = service.modificar(espiente); //le paso lo que viene despues del RequestBody
		return new ResponseEntity<Object>(HttpStatus.OK); //location es la url del recurso que usamos para esa insercion
	}
	
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public void eliminar(@PathVariable Integer id) {
	Especialidad esp = service.listarId(id);
	if (esp ==null) {
		throw new ModeloNotFoundException("ID: " + id); //en lugar de hacer lo de las excepciones por aca mejor enio el mensaje a la clase ModeloNotFoundException que los tienen todos 
	} else {
		service.eliminar(id);
		
	}
	}
	
	
	
}
