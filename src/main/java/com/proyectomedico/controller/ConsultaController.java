package com.proyectomedico.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.proyectomedico.dto.ConsultaListaExamenDTO;
import com.proyectomedico.exception.ModeloNotFoundException;
import com.proyectomedico.model.Consulta;
import com.proyectomedico.service.IConsultaService;

@RestController
@RequestMapping("/consultas") //el acceso a esta clase sera con un sustantivo slash Consultas 


public class ConsultaController {

	@Autowired
	private IConsultaService service; //para poder acceder a las operacioens de las capas inferiores, en relaidad estoy tomando la implementacion
	
	/*
	 * CON ESTE ORGININALMENTE FUNCIONA PERO SE PUEDE IMPLEMENTAR COMO ESTA ABAJO
	 * 
	@GetMapping (produces = MediaType.APPLICATION_JSON_VALUE) //indico que produce este servicio
	public List<Consulta> listar(){
		List<Consulta> Consultas = new ArrayList<>();
		Consultas = service.listar();
		return Consultas; 
	}
	*/
	
	@GetMapping (produces = MediaType.APPLICATION_JSON_VALUE) //indico que produce este servicio
	public ResponseEntity<List<Consulta>> listar(){ //clase rconsonseentity puedo manipular el estatus code del servicio, es un conjunto de numeros ok es 200
		List<Consulta> Consultas = new ArrayList<>();
		Consultas = service.listar();
		return new ResponseEntity<List<Consulta>>(Consultas, HttpStatus.OK); // es el 200
	}
	
	//listar por id
	@GetMapping(value = "/{id}")
	public Resource<Consulta> listarId(@PathVariable("id") Integer id) {
		Consulta cons = service.listarId(id);
		if (cons ==null) {
			throw new ModeloNotFoundException("ID: " + id); //en lugar de hacer lo de las excepciones por aca mejor envio el mensaje a la clase ModeloNotFoundException que los tienen todos 
		} 
		
	//a continuacion sera para implementar el HATEOAS
		
		Resource<Consulta> resource = new Resource<Consulta>(cons);
		ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).listarId(id));
		resource.add(linkTo.withRel("Consulta-resource"));
		
		//return new ResponseEntity<Consulta>(HttpStatus.OK); 
		return resource;
		
	}
	
	
	@PostMapping (consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)//consume es que este servicio recibe una trama json desde angular 
	public ResponseEntity<Object>registrar(@Valid @RequestBody ConsultaListaExamenDTO ConsultaDTO){ //valid que rconsete los constraint que definimos en la Entity // recivo los datos que seleccion en ConlsutaDTO, desde la vista solo esos dos 
		Consulta cons = new Consulta();
		cons = service.registrar(ConsultaDTO); //le paso lo que viene despues del RequestBody
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(cons.getIdConsulta()).toUri(); //definir la ruta que genera el recurso de Consulta
		return ResponseEntity.created(location).build(); //location es la url del recurso que usamos para esa insercion
	}
	
	
	@PutMapping (consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)//consume es que este servicio recibe una trama json desde angular 
	public ResponseEntity<Object>actualiza(@Valid @RequestBody Consulta Consulta){ //valid que rconsete los constraint que definimos en la Entity
		Consulta cons = new Consulta();
		cons = service.modificar(Consulta); //le paso lo que viene dconsues del RequestBody
		return new ResponseEntity<Object>(HttpStatus.OK); //location es la url del recurso que usamos para esa insercion
	}
	
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public void eliminar(@PathVariable Integer id) {
	Consulta cons = service.listarId(id);
	if (cons ==null) {
		throw new ModeloNotFoundException("ID: " + id); //en lugar de hacer lo de las excepciones por aca mejor enio el mensaje a la clase ModeloNotFoundException que los tienen todos 
	} else {
		service.eliminar(id);
		
	}
	}
	
	
	
}
