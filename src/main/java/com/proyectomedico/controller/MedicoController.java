package com.proyectomedico.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import com.proyectomedico.exception.ModeloNotFoundException;
import com.proyectomedico.model.Medico;
import com.proyectomedico.service.IMedicoService;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@RestController
@RequestMapping("/medicos")
public class MedicoController {
	
	@Autowired
	private IMedicoService service;
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Medico>> listar(){
		List<Medico> medicos = new ArrayList<>();		
		medicos = service.listar();
		return new ResponseEntity<List<Medico>>(medicos, HttpStatus.OK);
		
	}
	
	@GetMapping(value = "/{id}")
	public Resource<Medico> listarId(@PathVariable ("id") Integer id){
		Medico med = service.listarId(id);
		if (med ==null) {
			throw new ModeloNotFoundException("ID: "+ id);
		}
		Resource<Medico> resource = new Resource<Medico>(med);
		ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).listarId(id));
		resource.add(linkTo.withRel("medico-resource"));
		return resource;
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object>registrar(@Valid  @RequestBody Medico medico){
		Medico med = new Medico();
		med = service.registrar(medico);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(med.getIdMedico()).toUri(); //definir la ruta que genera el recurso de paciente
		return ResponseEntity.created(location).build();

	}
	
	@PutMapping (consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)//consume es que este servicio recibe una trama json desde angular 
	public ResponseEntity<Object>actualiza(@Valid @RequestBody Medico medico){ //valid que respete los constraint que definimos en la Entity
		Medico med = new Medico();
		med = service.modificar(medico); //le paso lo que viene despues del RequestBody
		return new ResponseEntity<Object>(HttpStatus.OK); //location es la url del recurso que usamos para esa insercion
	}
	
	
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public void eliminar(@PathVariable Integer id) {
	Medico med = service.listarId(id);
	if (med ==null) {
		throw new ModeloNotFoundException("ID: " + id); //en lugar de hacer lo de las excepciones por aca mejor enio el mensaje a la clase ModeloNotFoundException que los tienen todos 
	} else {
		service.eliminar(id);
		
	}
	}
	

}
