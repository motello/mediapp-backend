package com.proyectomedico.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.proyectomedico.exception.ModeloNotFoundException;
import com.proyectomedico.model.Signos;
import com.proyectomedico.service.ISignosService;

@RestController
@RequestMapping("/signos")
public class SignosController {
	
	
	@Autowired 
	private ISignosService service;
	
	
	@GetMapping (produces = MediaType.APPLICATION_JSON_VALUE) //indico que produce este servicio
	public ResponseEntity<List<Signos>> listar(){ //clase responseentity puedo manipular el estatus code del servicio, es un conjunto de numeros ok es 200
		List<Signos> signos = new ArrayList<>();
		signos = service.listar();
		return new ResponseEntity<List<Signos>>(signos, HttpStatus.OK); // es el 200
	}
	
	//listar por id
		@GetMapping(value = "/{id}")
		public Resource<Signos> listarId(@PathVariable("id") Integer id) {
			Signos sig = service.listarId(id);
			if (sig ==null) {
				throw new ModeloNotFoundException("ID: " + id); //en lugar de hacer lo de las excepciones por aca mejor envio el mensaje a la clase ModeloNotFoundException que los tienen todos 
			} 
			
			//a continuacion sera para implementar el HATEOAS
				
				Resource<Signos> resource = new Resource<Signos>(sig);
				ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).listarId(id));
				resource.add(linkTo.withRel("signos-resource"));
				
				//return new ResponseEntity<Paciente>(HttpStatus.OK); 
				return resource;
				
			}
	
	
		@PostMapping (consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)//consume es que este servicio recibe una trama json desde angular 
		public ResponseEntity<Object>registrar(@Valid @RequestBody Signos signos){ //valid que respete los constraint que definimos en la Entity
			Signos sig = new Signos();
			sig = service.registrar(signos); //le paso lo que viene despues del RequestBody
			URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(sig.getIdSignos()).toUri(); //definir la ruta que genera el recurso de paciente
			return ResponseEntity.created(location).build(); //location es la url del recurso que usamos para esa insercion
		}
		
		

		@PutMapping (consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)//consume es que este servicio recibe una trama json desde angular 
		public ResponseEntity<Object>actualiza(@Valid @RequestBody Signos signos){ //valid que respete los constraint que definimos en la Entity
			Signos sig = new Signos();
			sig = service.modificar(signos); //le paso lo que viene despues del RequestBody
			return new ResponseEntity<Object>(HttpStatus.OK); //location es la url del recurso que usamos para esa insercion
		}
		
		
		@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
		public void eliminar(@PathVariable Integer id) {
		Signos sig = service.listarId(id);
		if (sig ==null) {
			throw new ModeloNotFoundException("ID: " + id); //en lugar de hacer lo de las excepciones por aca mejor enio el mensaje a la clase ModeloNotFoundException que los tienen todos 
		} else {
			service.eliminar(id);
			
		}
		}
		
		

}
