package com.proyectomedico.service;

import com.proyectomedico.model.Paciente;

public interface IPacienteService extends ICRUD<Paciente> { //le paso el generico en este caso Paciente y recibe eso en T
	// en este caso heredo un interface ya que en esta interfaz IPacienteService puede que tenga otros metodos
	//ya propios de pacientes como listar por dpi etc, es decir metodos que no son parte del crud 
	//desacoplo el codigo
	
	

}
