package com.proyectomedico.service;

import com.proyectomedico.dto.ConsultaListaExamenDTO;
import com.proyectomedico.model.Consulta;

public interface IConsultaService extends ICRUD<Consulta>{

	Consulta registrar(ConsultaListaExamenDTO consultaDTO);
	
}
  