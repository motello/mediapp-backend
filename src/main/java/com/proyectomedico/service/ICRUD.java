package com.proyectomedico.service;

import java.util.List;

//AQUI VAN LOS CRUD COMUNES = = = para no redundar mucho codigo
public interface ICRUD<T>  { //investigar de genericos

	T registrar(T t); // IGUAL QUE: Paciente registrar(Paciente pac);
	
	T modificar(T t);
	
	void eliminar(int id);
	
	T listarId(int id);
	
	List<T> listar();
	
}
