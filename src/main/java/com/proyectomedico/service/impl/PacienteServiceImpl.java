package com.proyectomedico.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proyectomedico.dao.IPacienteDAO;
import com.proyectomedico.model.Paciente;
import com.proyectomedico.service.IPacienteService;


@Service //para que yo pueda hacer una @Autowired mas adelante de esta clase  
public class PacienteServiceImpl implements IPacienteService{

	@Autowired 
	private IPacienteDAO dao; // con esto tengo una instancia del DAO que tiene los metodos de guardar propios de JPA
	
	@Override
	public Paciente registrar(Paciente t) {
		return dao.save(t); //guardar la instancia
	}

	@Override
	public Paciente modificar(Paciente t) {
		return dao.save(t); //guardar la instancia
	}

	@Override
	public void eliminar(int id) {
		dao.delete(id);
		
	}

	@Override
	public Paciente listarId(int id) {
		return dao.findOne(id);
	}

	@Override
	public List<Paciente> listar() {
		return dao.findAll();
	}

	
	
	
}
