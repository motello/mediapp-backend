package com.proyectomedico.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proyectomedico.dao.IConsultaDAO;
import com.proyectomedico.dao.IConsultaExamenDAO;
import com.proyectomedico.dto.ConsultaListaExamenDTO;
import com.proyectomedico.model.Consulta;
import com.proyectomedico.service.IConsultaService;

@Service
public class ConsultaServiceImpl implements IConsultaService{

	@Autowired
	private IConsultaDAO dao;
	
	@Autowired
	private IConsultaExamenDAO cedao;
	
	@Override
	public Consulta registrar(Consulta t) {
		return dao.save(t);
	}

	@Override
	public Consulta modificar(Consulta t) {
		return dao.save(t);
	}

	@Override
	public void eliminar(int id) {
		dao.delete(id);
	}  

	@Override
	public Consulta listarId(int id) {
		return dao.findOne(id);
	}

	@Override
	public List<Consulta> listar() {
		return dao.findAll();
	}

	@Transactional//para que esta Query haga el commit - es decir para que se confirme la transaccion
	@Override
	public Consulta registrar(ConsultaListaExamenDTO consultaDTO) {
		//practicamente digo con la siguiente linea
		//que obtenga el detalle de la consulta y para cada detalla le meta la consulta, ya que tengo la relacion manytoone en detallconsulta en la clase misma
		//ahora necesito decirle al Json que no necesita le detalle consulta lo hago en la clase detalleConsulta con un @JsonIgnore
		consultaDTO.getConsulta().getDetalleConsulta().forEach(x -> x.setConsulta(consultaDTO.getConsulta())); //es para que dentro de la consulta que se hace en el json se incluya la consulta que tiene incluido el detalle, pero no hacerlo alla sino aqui
		dao.save(consultaDTO.getConsulta());
		consultaDTO.getLstExamen().forEach(e -> cedao.registrar(consultaDTO.getConsulta().getIdConsulta(), e.getIdExamen() )); //en el cedao.registrar realmente hago un insert into
		
		return consultaDTO.getConsulta();
	}

}
