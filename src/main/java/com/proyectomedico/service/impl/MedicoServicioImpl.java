package com.proyectomedico.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proyectomedico.dao.IMedicoDAO;
import com.proyectomedico.model.Medico;
import com.proyectomedico.service.IMedicoService;

@Service
public class MedicoServicioImpl implements IMedicoService{

	@Autowired
	private IMedicoDAO dao;
	
	
	@Override
	public Medico registrar(Medico t) {
		return dao.save(t);
	}

	@Override
	public Medico modificar(Medico t) {
		return dao.save(t);
	}

	@Override
	public void eliminar(int id) {
		dao.delete(id);
	}

	@Override
	public Medico listarId(int id) {
		return dao.findOne(id);
	}

	@Override
	public List<Medico> listar() {
		return dao.findAll();
	}

}
