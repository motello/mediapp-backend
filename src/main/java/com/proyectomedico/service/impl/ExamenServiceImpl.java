package com.proyectomedico.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proyectomedico.dao.IExamenDAO;
import com.proyectomedico.model.Examen;
import com.proyectomedico.service.IExamenService;


@Service //para que yo pueda hacer una @Autowired mas adelante de esta clase  
public class ExamenServiceImpl implements IExamenService{

	@Autowired 
	private IExamenDAO dao; // con esto tengo una instancia del DAO que tiene los metodos de guardar propios de JPA
	
	@Override
	public Examen registrar(Examen t) {
		return dao.save(t); //guardar la instancia
	}

	@Override
	public Examen modificar(Examen t) {
		return dao.save(t); //guardar la instancia
	}

	@Override
	public void eliminar(int id) {
		dao.delete(id);
		
	}

	@Override
	public Examen listarId(int id) {
		return dao.findOne(id);
	}

	@Override
	public List<Examen> listar() {
		return dao.findAll();
	}

	
	
	
}
