package com.proyectomedico.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
@Table(name="consulta")
public class Consulta {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer IdConsulta;
	
	@ManyToOne 
	@JoinColumn(name="id_paciente", nullable = false)
	private Paciente paciente;


	@ManyToOne
	@JoinColumn(name="id_medico", nullable = false)
	private Medico medico;

	@ManyToOne
	@JoinColumn(name="id_especialidad", nullable = false)
	private Especialidad especialidad;
	
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDateTime fecha;

	//como que estuvieramos viendo desde detalle hacia consulta
	
	@OneToMany(mappedBy = "consulta", cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.REMOVE}, fetch = FetchType.LAZY, orphanRemoval = true) //sirve para mapear listas
	private List<DetalleConsulta> detalleConsulta;
	 //LAZY optimiza la consulta despues se puede mandar a traer el numero de filas solicitadas tipo pageable
	//EAGLE traeria toda la data de golpe, todo el detalla
   //orphanRemoval a la lista implicada permite dejar huerfano a un hijo
	
	public Integer getIdConsulta() {
		return IdConsulta;
	}

	public void setIdConsulta(Integer idConsulta) {
		IdConsulta = idConsulta;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public Medico getMedico() {
		return medico;
	}

	public void setMedico(Medico medico) {
		this.medico = medico;
	}

	public Especialidad getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(Especialidad especialidad) {
		this.especialidad = especialidad;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}
	
	public List<DetalleConsulta> getDetalleConsulta() {
		return detalleConsulta;
	}

	public void setDetalleConsulta(List<DetalleConsulta> detalleConsulta) {
		this.detalleConsulta = detalleConsulta;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((IdConsulta == null) ? 0 : IdConsulta.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Consulta other = (Consulta) obj;
		if (IdConsulta == null) {
			if (other.IdConsulta != null)
				return false;
		} else if (!IdConsulta.equals(other.IdConsulta))
			return false;
		return true;
	}
	
	
	
	
	
	
	
}
