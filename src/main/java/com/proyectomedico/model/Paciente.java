package com.proyectomedico.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Informacion del paciente")
@Entity
@Table(name = "paciente") //en la BDD con minuscula estara
public class Paciente { //en la progra 
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idPaciente;
	
	//@JsonIgnore //para que no se muestre
	@ApiModelProperty(notes = "Nombres deber tener como minimo 3 caracteres")//no es propio de springboot
	//Size que la data sea integra, la que se espera por ejemplos
	@Size(min=3, message ="Nombres deber tener como minimo 3 caracteres ")//si no es de javax persistence no altera la base de datos, solamente a nivel logico de la apliacion
	@Column(name = "nombres", nullable = false, length = 70)//para base de datos
	private String nombres;//propio de java

	@ApiModelProperty(notes = "Apellidos deber tener como minimo 3 caracteres")//no es propio de springboot
	@Size(min=3, message ="Apellidos deber tener como minimo 3 caracteres ")//si no es de javax persistence no altera la base de datos, solamente a nivel logico de la apliacion
	@Column(name = "apellidos", nullable = false, length = 70)
	private String apellidos;

	@Size(min=13, max=13, message ="DPI deber tener 13 caracteres ")//si no es de javax persistence no altera la base de datos, solamente a nivel logico de la apliacion
	@Column(name = "dpi", nullable = false, length = 13)
	private String dpi;
	
	@Size(min=3, message ="Direccion deber tener como minimo 3 caracteres ")//si no es de javax persistence no altera la base de datos, solamente a nivel logico de la apliacion
	@Column(name = "direccion", nullable = true, length = 150)
	private String direccion;
	
	@Column(name = "telefono", nullable = false, length = 8)
	private String telefono;
	
	public Integer getIdPaciente() {
		return idPaciente;
	}
	public void setIdPaciente(Integer idPaciente) {
		this.idPaciente = idPaciente;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getDpi() {
		return dpi;
	}
	public void setDpi(String dpi) {
		this.dpi = dpi;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	

	
	
	

}
