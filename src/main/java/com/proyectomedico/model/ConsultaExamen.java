package com.proyectomedico.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity
@IdClass(ConsultaExamenPK.class)//con esto he indicado que los dos @Id que estan debajo, forman una llave primaria compuesta
//y alla esta la explicacion de lo dicho
public class ConsultaExamen {

	@Id
	private Examen examen;
	@Id
	private Consulta consulta;
	
	
	
	public Examen getExamen() {
		return examen;
	}
	public void setExamen(Examen examen) {
		this.examen = examen;
	}
	public Consulta getConsulta() {
		return consulta;
	}
	public void setConsulta(Consulta consulta) {
		this.consulta = consulta;
	}
	
	
}
