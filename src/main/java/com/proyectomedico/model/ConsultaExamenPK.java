package com.proyectomedico.model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

//clase que tiene la representacion del comportamiento de la llave foreanea
//en esta establezco como es su relacion de la llave foranea en cuanto a usar @ManyToOne
//en la otra indico que es un @Id
//es una buena practica es bueno tenerlo en una clase como esta que es embebida

@Embeddable
//clase que no maneja srping por eso le colocamos serializable
public class ConsultaExamenPK implements Serializable {
	
	@ManyToOne
	@JoinColumn(name="idExamen", nullable = false)
	private Examen examen;
	
	
	@ManyToOne
	@JoinColumn(name="idConsulta", nullable = false)
	private Consulta consulta;


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((consulta == null) ? 0 : consulta.hashCode());
		result = prime * result + ((examen == null) ? 0 : examen.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConsultaExamenPK other = (ConsultaExamenPK) obj;
		if (consulta == null) {
			if (other.consulta != null)
				return false;
		} else if (!consulta.equals(other.consulta))
			return false;
		if (examen == null) {
			if (other.examen != null)
				return false;
		} else if (!examen.equals(other.examen))
			return false;
		return true;
	}

	//entonces necesito compararar valores como los que tiene cada objeto en este caso son dos objetos como tal, 
	//pero no es necesario hacer un objeto.getValor, sino que lo hago con el metodo equals y hashcode para comparar
	//datos sin instanciar cada objeto.
	
	
	
}
