package com.proyectomedico.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="detalle_consulta")
public class DetalleConsulta {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer IdDetalle;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="id_consulta", nullable = false) //para hacer referencia a la clave foranea que indica esta linea
	private Consulta consulta;
	
	@Column(name="diagnostico", nullable = false, length = 70)
	private String diagnostico; 
	
	@Column(name="tratamiento", nullable = false, length = 300)
	private String tratamiento;
	
	public Integer getIdDetalle() {
		return IdDetalle;
	}
	public void setIdDetalle(Integer idDetalle) {
		IdDetalle = idDetalle;
	}
	public Consulta getConsulta() {
		return consulta;
	}
	public void setConsulta(Consulta consulta) {
		this.consulta = consulta;
	}
	public String getDiagnostico() {
		return diagnostico;
	}
	public void setDiagnostico(String diagnostico) {
		this.diagnostico = diagnostico;
	}
	public String getTratamiento() {
		return tratamiento;
	}
	public void setTratamiento(String tratamiento) {
		this.tratamiento = tratamiento;
	}
	
	
}
