package com.proyectomedico.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proyectomedico.model.Especialidad;

public interface IEspecialidadDAO extends JpaRepository<Especialidad, Integer> {

	//tengo los metodos para guardar etc
	
}
