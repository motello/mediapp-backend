package com.proyectomedico.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.proyectomedico.model.ConsultaExamen;

public interface IConsultaExamenDAO extends JpaRepository<ConsultaExamen, Integer> {

	//Se puede usar persist, utilizar un Query para tener la flexibilidad para utilizar un Query
	
	//haremos una insercion en la tabla intermedia, la de muchos a muchos de consulta y examen
	
	//@Transactional//para que esta Query haga el commit - es decir para que se confirme la transaccion
	@Modifying //para hacer querys DML ya si son Querys propios como de SQL select etc... no es necesario
	//DML | insert, update ,delete | SQL select
	@Query(value = "INSERT INTO consulta_examen(id_consulta, id_examen) VALUES (:idConsulta, :idExamen)", nativeQuery = true)//estoy haciendo referencia a la "TABLA"
	Integer registrar(@Param("idConsulta")Integer idConsulta, @Param("idExamen")Integer idExamen);//aqui obtengo y guardo en el Query de arriba
	
}
