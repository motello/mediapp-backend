package com.proyectomedico.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proyectomedico.model.Examen;

public interface IExamenDAO extends JpaRepository<Examen, Integer> {

	//tengo los metodos para guardar etc con el JpaRepository
	
}
