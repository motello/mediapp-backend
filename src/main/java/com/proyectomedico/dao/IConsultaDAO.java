package com.proyectomedico.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proyectomedico.model.Consulta;

public interface IConsultaDAO extends JpaRepository<Consulta, Integer>{

}
 