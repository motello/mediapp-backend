package com.proyectomedico.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proyectomedico.model.Signos;

public interface ISignosDAO extends JpaRepository<Signos, Integer>{

}
